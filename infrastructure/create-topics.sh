#!/bin/bash

KAFKA_BIN="/opt/bitnami/kafka/bin"
KAFKA_SERVER="kafka:9092"

"${KAFKA_BIN}/kafka-topics.sh" --bootstrap-server "${KAFKA_SERVER}" --list

RETENTION_MS="$((60*1000*60*24))"
TRADING_ACTIONS_TOPIC="trading-actions"
CANDLES_TOPIC="candle-info"
INSTANT_CONTEXT="instant-context"
ACCOUNT_UPDATE_TOPIC="account-update"
PROCESSED_ACTIONS_TOPIC="processed-actions"
ACTIONS_SORTER_TOPIC="actions-sorter"

echo -e 'Creating kafka topics'
"${KAFKA_BIN}/kafka-topics.sh" --bootstrap-server "${KAFKA_SERVER}" --create --if-not-exists \
  --topic "${TRADING_ACTIONS_TOPIC}" \
  --config retention.ms="${RETENTION_MS}"
"${KAFKA_BIN}/kafka-topics.sh" --bootstrap-server "${KAFKA_SERVER}" --create --if-not-exists \
  --topic "${CANDLES_TOPIC}" \
  --partitions "${CANDLES_PARTITIONS}" \
  --config retention.ms="${RETENTION_MS}"
"${KAFKA_BIN}/kafka-topics.sh" --bootstrap-server "${KAFKA_SERVER}" --create --if-not-exists \
  --topic "${INSTANT_CONTEXT}" \
  --config retention.ms="${RETENTION_MS}"
"${KAFKA_BIN}/kafka-topics.sh" --bootstrap-server "${KAFKA_SERVER}" --create --if-not-exists \
  --topic "${PROCESSED_ACTIONS_TOPIC}" \
  --config retention.ms="${RETENTION_MS}"
"${KAFKA_BIN}/kafka-topics.sh" --bootstrap-server "${KAFKA_SERVER}" --create --if-not-exists \
  --topic "${ACTIONS_SORTER_TOPIC}" \
  --config retention.ms="${RETENTION_MS}"
"${KAFKA_BIN}/kafka-topics.sh" --bootstrap-server "${KAFKA_SERVER}" --create --if-not-exists \
  --topic "${ACCOUNT_UPDATE_TOPIC}" \
  --config retention.ms="${RETENTION_MS}"

echo -e 'Successfully created the following topics:'
"${KAFKA_BIN}/kafka-topics.sh" --bootstrap-server "${KAFKA_SERVER}" --list
